import {inject} from 'aurelia-framework'
import * as environment from '../config/environment.json'
import {HttpClient} from 'aurelia-fetch-client'

@inject(HttpClient)
export class App {
  constructor(httpClient) {
    this.message = 'Hello World!'
    this.urlBackend = environment.url
    this.http = httpClient
    this.personas = []
    this.gitProperties = {
      url: "",
      user: "",
      password: ""
    }
    this.cicdProperties = {}
  }

  created() {
    this.http.fetch(`${this.urlBackend}/personas.json`)
      .then(response => response.json())
      .then(personas => this.personas = personas)

    // this.http.fetch('/crear', {
    //   method: 'post',
    //   headers: {
    //     'Content-Type': 'application/json',
    //     'Accept': 'application/json'
    //   },
    //   body: JSON.stringify({
    //     gitProperties: this.gitProperties,
    //     cicdProperties: this.cicdProperties
    //    })
    // })
  }

}

// return this.http.fetch('auth', {
//   method: 'post',
//   body: JSON.stringify(auth),
//   headers: {
//     'Content-Type': 'application/json',
//     'Accept': 'application/json'
//   }
// })
// .then(response => response.json())
// .then(response => {
//   this.apiKey = response.APIKey;
//   console.log(response);
// });